﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1
{
    [TestClass]
    public class Sum
    {
        [TestMethod]
        public void Parse()
        {
            var t = new Parser();
            t.str = "3+3";
            var res = t.Parse();
            Assert.AreEqual(6, res);
        }

        [TestMethod]
        public void Parse1()
        {
            var t = new Parser();
            t.str = "3+(1+2)";
            var res = t.Parse();
            Assert.AreEqual(6, res);
        }

        [TestMethod]
        public void Parse2()
        {
            var t = new Parser();
            t.str = "3+0";
            var res = t.Parse();
            Assert.AreEqual(3, res);
        }

        public void ParseM()
        {
            var t = new Parser();
            t.str = "6+(-3)";
            var res = t.Parse();
            Assert.AreEqual(-3, res);
        }
    }

    [TestClass]
    public class Minus
    {
        [TestMethod]
        public void Parse()
        {
            var t = new Parser();
            t.str = "6-3";
            var res = t.Parse();
            Assert.AreEqual(3, res);
        }

        [TestMethod]
        public void ParseM()
        {
            var t = new Parser();
            t.str = "-3";
            var res = t.Parse();
            Assert.AreEqual(-3, res);
        }

        [TestMethod]
        public void ParseMM()
        {
            var t = new Parser();
            t.str = "6--3";
            var res = t.Parse();
            Assert.AreEqual(9, res);
        }

        [TestMethod]
        public void Parse1()
        {
            var t = new Parser();
            t.str = "6-0";
            var res = t.Parse();
            Assert.AreEqual(6, res);
        }
    }

    [TestClass]
    public class Mult
    {
        [TestMethod]
        public void Parse()
        {
            var t = new Parser();
            t.str = "3*2";
            var res = t.Parse();
            Assert.AreEqual(6, res);
        }

        [TestMethod]
        public void Parse0()
        {
            var t = new Parser();
            t.str = "3*0";
            var res = t.Parse();
            Assert.AreEqual(0, res);
        }
    }

    [TestClass]
    public class Div
    {
        [TestMethod]
        public void Parse()
        {
            var t = new Parser();
            t.str = "6/2";
            var res = t.Parse();
            Assert.AreEqual(3, res);
        }

        [TestMethod]
        public void Parse0()
        {
            var t = new Parser();
            t.str = "6/0";
            var res = t.Parse();
            Assert.AreEqual(0, res);
        }
    }

    [TestClass]
    public class Test1
    {
        [TestMethod]
        public void Parse1()
        {
            var t = new Parser();
            t.str = "(6+1)+3";
            var res = t.Parse();
            Assert.AreEqual(10, res);
        }

        [TestMethod]
        public void Parse2()
        {
            var t = new Parser();
            t.str = "(6+2+3)";
            var res = t.Parse();
            Assert.AreEqual(11, res);
        }

        [TestMethod]
        public void Parse3()
        {
            var t = new Parser();
            t.str = "(6+2+3";
            var res = t.Parse();
            Assert.AreEqual(0, res);
        }

        [TestMethod]
        public void Parse4()
        {
            var t = new Parser();
            t.str = ")6+2+3";
            var res = t.Parse();
            Assert.AreEqual(0, res);
        }

        [TestMethod]
        public void Parse6()
        {
            var t = new Parser();
            t.str = "6*2+3)";
            var res = t.Parse();
            Assert.AreEqual(15, res);
        }

        [TestMethod]
        public void Parse7()
        {
            var t = new Parser();
            t.str = "6";
            var res = t.Parse();
            Assert.AreEqual(6, res);
        }
    }

    public class Parser
    {
        public int c = 0;
        public int counter = 0;
        public bool Error = false;
        public string str;

        public void Get()
        {
            if (counter < str.Length) c = str[counter++];
            else c = 10;
        }

        public double ProcC()
        {
            double x = 0;
            double x1 = 0;
            while (c >= '0' && c <= '9')
            {
                x *= 10;
                x += c - '0';
                Get();
            }
            if (c == ',')
            {
                Get();
                while (c >= '0' && c <= '9')
                {
                    x1 *= 10;
                    x1 += c - '0';
                    Get();
                }
                while (x1 > 1)
                    x1 /= 10;

                x += x1;
            }
            return x;
        }

        public double ProcM()
        {
            double x = 0;

            if (c == '(')
            {
                Get();
                x = ProcE();
                if (c != ')')
                {
                    Error = true;
                    return 0;
                }
                else Get();
            }
            else
              if (c == '-')
            {
                Get();
                x = -ProcM();
            }
            else
                  if (c >= '0' && c <= '9')
                x = ProcC();
            else
            {
                Error = true;
                return 0;//Error("Syntax error.", 0);
            }
                

            return x;
        }

        public double ProcT()
        {
            double x = ProcM();
            while (c == '*' || c == '/')
            {
                char p = (char)c;
                Get();
                if (p == '*')
                    x *= ProcM();
                else
                {
                    double y = ProcM();

                    if (y == 0)
                    {
                        Error = true;
                        x = 0;
                    }
                    else x /= y;
                }
            }
            return x;
        }

        public double ProcE()
        {
            double x = ProcT();
            while (c == '+' || c == '-')
            {
                char p = (char)c;
                Get();
                if (p == '+')
                    x += ProcT();
                else
                    x -= ProcT();
            }
            return x;
        }

        public double ProcS()
        {
            Get();
            double p = ProcE();

            return p;
        }

        public double Parse()
        {
            double result = 0;
            result = ProcS();

            return result;
        }
    }
}

