﻿using System;
using System.Windows;
using System.Windows.Input;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnitTestProject1;

namespace Calc
{
    public class MainViewModel : INotifyPropertyChanged, IDataErrorInfo
    {
        public string X { get { return _result; } set { _result = value; OnPropertyChanged(nameof(X)); } }

        private string _result = "";
        public ObservableCollection<string> Memory { get; set; }

        private string item;
        public string Item1
        {
            get { return item; }
            set
            {
                item = value;
                OnPropertyChanged("Item1");
            }
            
        }

        public string this[string columnName]
        {
            get
            {
                char prev = ' ';
                string error = String.Empty;

                for (int i = 0; i < X.Length; i++)
                {
                    if (Char.IsDigit(X, i) || X[i] == ',' || X[i] == '+' || X[i] == '-' || X[i] == '*' || X[i] == '/' || X[i] == '(' || X[i] == ')')
                    {
                        if (prev == '/' || prev == '*' || prev == ',' || prev == '+' || prev == '-')
                        {
                            if (X[i] == '*' || X[i] == '/')
                            {
                                error = "Error";
                                return error;
                            }

                        }

                        prev = X[i];
                    }
                    else
                    {
                        error = "Error";
                        break;
                    }

                    
                }
                
                return error;
            }
        }
        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        private void OnPropertyChanged(string PropertyName)
        {
            if (PropertyChanged == null) return;

            PropertyChanged.Invoke(this, new PropertyChangedEventArgs(PropertyName));
        }

        private ICommand _resultCommand = new Result();
        public ICommand ResultCommand { get { return _resultCommand; } }

        private ICommand _addtoOut;
        public ICommand AddToOut { get { return _addtoOut; } }

        private ICommand _remfromOut;
        public ICommand RemoveFromOut { get { return _remfromOut; } }

        private ICommand _addtoMem;
        public ICommand AddToMem { get { return _addtoMem; } }

        private ICommand _delfromMem;
        public ICommand DelFromMem
        {
            get
            {
                return _delfromMem ??
                  (_delfromMem = new RelayCommand<string>(obj =>
                  {
                          Memory.Remove(obj);
                          Item1 = null;
                  }, (obj) => Memory.Count > 0));
            }
        }

        private ICommand _loadfromMem;
        public ICommand LoadFromMem
        {
            get
            {
                return _loadfromMem ??
                  (_loadfromMem = new RelayCommand<string>(obj =>
                  {
                      X = Item1;
                  }, (obj) => Memory.Count > 0));
            }
        }

        private ICommand _memPlus;
        public ICommand MemPlus
        {
            get
            {
                return _memPlus ??
                  (_memPlus = new RelayCommand<string>(obj =>
                  {
                      if (Memory.Count == 0) Memory.Add(obj);
                      else Memory[Memory.IndexOf(obj)] = (Convert.ToDouble(Item1) + Convert.ToDouble(X)).ToString();
                  }, (obj) =>
                  {
                      if (X.Length == 0) return false;
                      for (int i = 0; i < X.Length; i++)
                      {
                          if (Char.IsDigit(X, i) || X[i] == ',') continue;
                          else return false;
                      }
                      return true;
                  }));
            }
        }

        private ICommand _memMinus;
        public ICommand MemMinus
        {
            get
            {
                return _memMinus ??
                  (_memMinus = new RelayCommand<string>(obj =>
                  {
                      Memory[Memory.IndexOf(obj)] = (Convert.ToDouble(Item1) - Convert.ToDouble(X)).ToString();
                  }, (obj) =>
                  {
                      if (X.Length == 0) return false;
                      for (int i = 0; i < X.Length; i++)
                      {
                          if (Char.IsDigit(X, i) || X[i] == ',') continue;
                          else return false;
                      }
                      return true;
                  }));
            }
        }


        public MainViewModel()
        {
            Memory = new ObservableCollection<string> { };
            _addtoOut = new RelayCommand<string>(x => X += x);
            _addtoMem = new RelayCommand<string>(obj => { Item1 = obj; Memory.Add(obj); }, (obj) => 
            {
                if (obj.Length == 0) return false;
                for (int i = 0; i < obj.Length; i++)
                {
                    if (Char.IsDigit(obj, i) || obj[i] == ',') continue;
                    else return false;
                }
                return true;
            });
            _remfromOut = new RelayCommand<string>(x =>
            {
                if (x == "C")
                    X = "";
                else
                    X = X.Remove(X.Length - 1);
            }, (x) => X.Length > 0);
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }

    public class Result : ICommand
    {
        public void Execute(object parameter)
        {
            var mainViewModel = parameter as MainViewModel;
            var res = new Parser();
            res.str = mainViewModel.X;

            double result = res.Parse();
            if (res.Error) mainViewModel.X = "Error";
            else
                mainViewModel.X = result.ToString(); 
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public bool CanExecute(object parameter)
        {
            var mainViewModel = parameter as MainViewModel;

            if (mainViewModel == null)
                return true;

            return string.IsNullOrEmpty(mainViewModel.X) == false;
        }
    }

    public class RelayCommand<T> : ICommand
    {
        private readonly Action<T> _execute;
        private readonly Func<T, bool> _canExecute;

        public RelayCommand(Action<T> execute, Func<T, bool> canExecute = null)
        {
            _execute = execute ?? throw new ArgumentNullException(nameof(execute));
            _canExecute = canExecute;
        }

        public void Execute(object parameter)
        {
            if (parameter is T arg)
            {
                _execute.Invoke(arg);
            }
        }

        public bool CanExecute(object parameter)
        {
            if (parameter is T arg)
            {
                return _canExecute?.Invoke(arg) ?? true;
            }
            return false;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }
    }
}
